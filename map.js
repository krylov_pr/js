const person = {
    name: 'henderson',
    age: 25
}

let p = [
    ['name', 'henderson'],
    ['age', 25]
]

let maps = new Map(p)

maps.set(person, 50);
console.log(maps.get(person))

// for(let [key, value] of maps.entries()) {
//     console.log(key, value)
// }

// for(let val of maps.values()) {
//     console.log(val)
// }

// for(let key of maps.keys()) {
//     console.log(key)
// }

maps.forEach((value, key) => {
    console.log(`${key} ${value}`)
})


/*
* Set
* */

let arr = [0,1,1,1,2,3,3,3,4]
let arrSet = new Set(arr) // удаляет дублирующие элементы
arrSet.add(33).add(20).add(33) // дублирующих элементов не будет
console.log(arrSet)

/*
* WeakMap
* */

const users = [{name: 'name1'}, {name: 'name2'}, {name: 'name3'}]
const uArr = users
let usersWakeMap = new WeakMap()
function cashing(el) {
    if(!usersWakeMap.has(el)) {
        usersWakeMap.set(el, Date.now())
    }
    return usersWakeMap.get(el)
}
cashing(users[0])
cashing(users[1])
users.splice(1, 1)
console.log(usersWakeMap.has(users[0]))
console.log(usersWakeMap.has(users[1])) // будет false, т.к. удаленный объект также удаляется и из WeakMap


