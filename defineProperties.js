let user = Object.create({
    updateUser() {
        console.log('update user')
    }
}, {
    name: {
        value: 'Henderson',
        enumerable: true
    },
    date: {
        value: 1992,
        writable: true,
        enumerable: true,
        configurable: true
    },
    age: {
        set(value) {
            console.log('update date')
        },
        get() {
            return new Date().getFullYear() - this.date
        }
    }
})
user.age = 25
for(let u in user) {
    if(user.hasOwnProperty(u)) {
        console.log(u, user[u])
    }
}
