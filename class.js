/*
* Class
* */
class F {
    constructor(props) {
        this.name = props.name
    }
    setName(name) {
        this.name = name
    }
    getName() {
        return this.name
    }
}
class F2 extends F {
    constructor(props) {
        super(props)
        this.age = props.age
    }

    set setAge(age) {
        this.age = age
    }
    get getAge() {
        return this.age
    }
    set setName(name) {
        console.log('update name')
        super.setName(name)
    }
}

let user = new F({
    name: 'name1'
})
let user2 = new F2({
    name: 'name2',
    age: 27
})
Object.defineProperties(user2, {
    getAge: {
        value: user2.getAge,
        enumerable: true
    }
})
user2.setName = 'new user2'

class Component {
    constructor(selector) {
        this.$el = document.querySelector(selector)
    }
    show() {
        this.$el.style.display = 'block'
    }
    hide() {
        this.$el.style.display = 'none'
    }
}
class Block extends Component {
    constructor(options) {
        super(options.selector)
        this.$el.style.width = this.$el.style.height = options.size + 'px'
        this.$el.style.background = options.color
    }
}

class Circle extends Block {
    constructor(options) {
        super(options)
        this.$el.style.borderRadius = '50%'
    }
}

let b1 = new Block({
    selector: '#block1',
    size: 100,
    color: 'red'
})
let c = new Circle({
    selector: '#block2',
    size: 80,
    color: 'green'
})
