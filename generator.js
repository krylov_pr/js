function* func() {
    yield 'H'
    yield 'E'
    yield 'N'
    yield 'D'
    yield 'E'
    yield 'R'
    yield 'S'
    yield 'O'
    yield 'N'
}
const f = func()

const iter = {
    gen(n = 10) {
        let index = 0
        return {
            next() {
                if(index <= 10) {
                    return {value: ++index, dane: false}
                } else {
                    return {value: undefined, dane: true}
                }
            }
        }
    }
}
const it = iter.gen()

function* iterator(n = 10) {
    for (let i = 0; i <= n; i++) {
        yield i
    }
}
const resultIterator = iterator()

