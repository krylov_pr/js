const inpFile = document.querySelector('#inpFile')
const imgBase = document.querySelector('#imageBase')
const fileReader = new FileReader()

fileReader.addEventListener('load', (e) => {
    imgBase.src = e.target.result
})

inpFile.addEventListener('change', (e) => {
    const [file] = e.target.files

    if(file) {
        if(file.size > 300 * 1024) {
            alert('слишком большой файл')
        } else {
            fileReader.readAsDataURL(file)
        }
    }
})
