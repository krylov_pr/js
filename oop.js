function inherit(child, parent) {
    child.prototype = Object.create(parent.prototype)
    child.prototype.parent = parent
    // let Temp = function() {}
    // Temp.prototype = parent.prototype
    // child.prototype = new Temp()
}

function F(name) {
    this.name = name
}
function F2(name, age) {
    this.name = name
    this.age = age
}

inherit(F2, F)

let user = new F('Henderson')
let user2 = new F2('Henderson2', 25)
F.prototype.setName = function(newName) {
    this.name = newName
}
F.prototype.getName = function() {
    return this.name
}
F2.prototype.setAge = function(age) {
    this.age = age
}
F2.prototype.getAge = function() {
    return this.age
}
F2.prototype.setName = function(name) {
    console.log('[set name]')
    this.parent.prototype.setName.call(this, name);
}
