enum MemberShip {
    Lite, // 0
    Standard, // 1
    Premium// 2
}
const membership = MemberShip.Standard
console.log(membership) // 1

enum Social {
    VK = 'VK',
    FACEBOOK = 'FACEBOOK'
}
const social = Social.FACEBOOK
console.log(social) // FACEBOOK