interface Rect {
    readonly id: string, // readonly -  только для чтения - невозможность изменения
    styles?: string, // ? - означает что поле необязательное
    size: {
        width: number,
        height: number
    }
}

const rect1: Rect = {
    id: 'rect-1',
    size: {
        width: 20,
        height: 10
    }
}
rect1.styles = 'color: red'

interface Rect2 extends Rect {
    name: string
    getArea: () => number
}

const rect2: Rect2 = {
    id: 'rect-2',
    size: {
        width: 20,
        height: 10
    },
    name: 'new rect 2',
    getArea (): number {
        return this.width * this.height
    }
}

const rect3 = {} as Rect
const rect4 = <Rect>{}

interface IClock {
    time: Date,
    setTime(date: Date): void
}

class Clock implements IClock {
    time: Date = new Date()
    setTime(date: Date) {
        this.time = date
    }
}

interface Styles {
    [key: string]: string // ко всем ключам типа стринг будет присвоен тип стринг
}

const css: Styles = {
    border: '1px solid black',
    marginTop: '2px'
}