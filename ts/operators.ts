interface Person {
    name: string
    age: number
}

type PersonKey = keyof Person // 'name' | 'age'

let key: PersonKey = 'age'
let key2: PersonKey = 'name'
// let key3: PersonKey = 'job' // error - такого ключа нет

