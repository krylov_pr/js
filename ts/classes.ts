class Car {
    readonly model: string
    readonly numberOfWheels: number = 4
    constructor(theModel: string) {
        this.model = theModel
    }
}

/*------*/
// класс Car можно записать более лаконично
class Car2 {
    readonly numberOfWheels: number = 4
    constructor(readonly model: string) {}
}

//=============
class Animal {
    protected voice: string = '' // доступен внутри класса Animal и внутри всех дочерних классах
    public color: string = 'black' // по умолчанию все метоты public
    constructor() {
        this.go()
    }
    private go() { // доступен только в текущем классе
        console.log('Go')
    }
}
class Cat extends Animal {
    public setVoice(voice: string): void {
        this.voice = voice
    }
}
const cat = new Cat()
cat.setVoice('test')
console.log(cat)