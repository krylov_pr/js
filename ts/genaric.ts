const arrayOfNumbers: Array<number> = [1, 2, 3]

// Тип T позволяет обрабатывать разные типы в массиве
function reverse<T>(array: T[]): T[] {
    return array.reverse()
}