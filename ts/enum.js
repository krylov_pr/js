var MemberShip;
(function (MemberShip) {
    MemberShip[MemberShip["Lite"] = 0] = "Lite";
    MemberShip[MemberShip["Standard"] = 1] = "Standard";
    MemberShip[MemberShip["Premium"] = 2] = "Premium"; // 2
})(MemberShip || (MemberShip = {}));
var membership = MemberShip.Standard;
console.log(membership); // 1
var Social;
(function (Social) {
    Social["VK"] = "VK";
    Social["FACEBOOK"] = "FACEBOOK";
})(Social || (Social = {}));
var social = Social.FACEBOOK;
console.log(social);
