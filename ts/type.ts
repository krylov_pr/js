const sts: string = 'hello'

const n: number = 42

const numberArray: number[] = [1,2,3,4]
const numberArray2: Array<number> = [1,2,3,4]

//Tuple
const contact: [string, number] = ['Pavel', 1234567]

// Any
let variable: any = 42
variable = 'New string'
variable = []

// ---
function sayMyName (name: string): void { // void - функция ничего не вернет
    console.log(name)
}

// Type

type Login = string
const login: Login = 'admin'

type Id = number | string

const id: Id = 12
const id2: Id = '12'
// const id3: Id = true // false

type SomeType = string | null | undefined