function random(from, to) {
    return parseInt(from + Math.random() * to - from)
}

let currentDiv = false
let startX = 0
let startY = 0

document.addEventListener('mousemove', (e) => {
    if(currentDiv) {
        currentDiv.style.top = e.clientY - startY + 'px'
        currentDiv.style.left = e.clientX - startX + 'px'
    }
})


function createDiv () {
    let div = document.createElement('div')
    const minSize = 80
    const maxSize = 200
    const maxColor = 0xffffff

    div.className = 'drag-block'
    div.style.background = '#' + random(0, maxColor).toString(16)
    div.style.top = random(0, window.innerHeight) + 'px'
    div.style.left = random(0, window.innerWidth) + 'px'
    div.style.width = random(minSize, maxSize) + 'px'
    div.style.height = random(minSize, maxSize) + 'px'

    div.addEventListener('mousedown', (e) => {
        currentDiv = div
        startX = e.offsetX
        startY = e.offsetY
    })
    div.addEventListener('mouseup', () => {currentDiv = false})

    return div
}

const btnCreateDiv = document.querySelector('#add-drag')
const wrapDrag = document.querySelector('#wrap-drag')

btnCreateDiv.addEventListener('click', () => {
    const div = createDiv()
    wrapDrag.appendChild(div)
})
