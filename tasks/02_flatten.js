function flatten(array) {
    const res = []
    array.map(el => {
        if(Array.isArray(el)) {
            const flat = flatten(el)
            flat.map(fl => {
                res.push(fl)
            })
        } else {
            res.push(el)
        }
    })
    return res
}

console.log(flatten([[1], [[2, 3]], [[[4]]]])) // -> 1, 2, 3, 4