function bind(fn, context, ...rest) {
    return function(...args) {
        const uniqId = Date.now().toString()
        context[uniqId]= fn
        const result = context[uniqId](...rest.concat(args))
        delete context[uniqId]
        return result
    }
}

const user = {
    name: 'Pavel'
}

function getUser(email, phone) {
    console.log(`Name: ${this.name}, Email: ${email}, Phone: ${phone}`)
}

bind(getUser, user, 'pavel@mail.ru', 12345)()
bind(getUser, user, 'pavel@mail.ru')(12345)
bind(getUser, user)('pavel@mail.ru', 12345)