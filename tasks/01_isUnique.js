function isUniqueOld(string) {
    const set = new Set()

    for(let i = 0; i < string.length; i++) {
        const char = string[i]

        if(set.has(char)) {
            return false
        }

        set.add(char)
    }

    return true
}

function isUnique (string) {
    return new Set(string).size === string.length
}

console.log(isUnique('abcde'))
console.log(isUnique('123456'))
console.log(isUnique('abacde'))
console.log(isUnique('1234356'))