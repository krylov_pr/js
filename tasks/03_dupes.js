function dupes(str) {
    // const res = []
    // const map = {}
    //
    // for(let i = 0; i < str.length; i++) {
    //     const char = str[i]
    //
    //     if(!map[char]) {
    //         map[char] = true
    //         res.push(char)
    //     }
    // }
    //
    // return res.join('')
    return Array.from(new Set(str)).join('')
}

console.log(dupes('abc')) // -> 'a,b,c'
console.log(dupes('abcabc')) // -> 'a,b,c'
console.log(dupes('abccba')) // -> 'a,b,c'
console.log(dupes('abccccaaabb')) // -> 'a,b,c'