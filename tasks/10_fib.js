/*
* Функция выводит последовательность чисел Фибоначчи. Аргумент - число чисел.
* Если аргумент следующего вызова меньше предыдущего, выводить результаты предыдущих вычислений
* */

const fibonacci = (function() {
    const sec = [1, 1]

    return function(n) {
        if (sec.length >= n) {
            console.log('no computed')
            return sec.slice(0, n)
        }

        while (sec.length < n) {
            const last = sec[sec.length - 1]
            const prev = sec[sec.length - 2]
            sec.push(last + prev)
        }
        return sec
    }
})()

console.log(fibonacci(10))
console.log(fibonacci(8))