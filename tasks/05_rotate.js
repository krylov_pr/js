function issRotate(source, test) {
    // if(source.length !== test.length) {
    //     return false
    // }
    //
    // for(let i = 0; i < source.length; i++) {
    //     const rotate = source.slice(i, source.length) + source.slice(0, i)
    //
    //     if(rotate === test) {
    //         return true
    //     }
    // }
    //
    // return false
    return (source + source).includes(test) && source.length === test.length
}

console.log(issRotate('javascript', 'scriptjava')) // -> true
console.log(issRotate('javascript', 'ascriptjav')) // -> true
console.log(issRotate('javascript', 'java')) // -> false