function frequency(array) {
    let max = 0
    const map = {}
    let maxStr = array[0]

    for (let i = 0; i < array.length; i++) {
        const item = array[i]

        if(!map[item]) {
            map[item] = 1
        } else {
            map[item]++
        }

        if(map[item] > max) {
            max = map[item]
            maxStr = item
        }
    }


    return maxStr
}

console.log(frequency(['a', 'b', 'c', 'c', 'c', 'c', 'd', 'd'])) // -> c
console.log(frequency(['abc', 'def', 'def', 'log', 'dev', 'abc', 'dev', 'dev'])) // -> dev
console.log(frequency(['a', 'b', 'c', 'c', 'c', 'd', 'd', 'a', 'a'])) // -> a