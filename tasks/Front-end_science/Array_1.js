// Написать функцию, которая выведет пересечения двух массивом.
// Количество повторение одного числа может быть несколько.
// В результирующем массиве это число должно быть столько раз, сколько есть повторений
const arr1 = [1, 2, 3, 4, 2, 3, 3]
const arr2 = [2, 2, 3, 7, 3, 4, 4, 4]

function arrResult (input1, input2) {
    const result = []

    const map = input1.reduce((acc, item) => {
        acc[item] = acc[item] ? acc[item] + 1 : 1
        return acc
    }, {})

    for(let i = 0; i < input2.length; i++) {
        const current = input2[i]

        if(map[current] && map[current] > 0) {
            result.push(current)
            map[current] -= 1
        }
    }

    return result
}

console.log(arrResult(arr1, arr2))