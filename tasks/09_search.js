/*
* Найти индекс элемента в массиве. Используя нелинейный алгоритм.
* */

function search(arr, target) {
    let start = 0
    let end = arr.length - 1

    if (target < arr[start] || target > arr[end]) {
       return -1
    }

    while (true) {
        if (end - start <= 1) {
            return -1
        }

        let middle = Math.floor((start + end) / 2)

        if (target > arr[middle]) {
            start = middle + 1
        } else if (target < arr[middle]) {
            end = middle - 1
        } else {
            return middle
        }
    }
}

console.log(search([1,3,5,9,13,18], 5)) // 2
console.log(search([1,3,5,9,13,18], 13)) // 4