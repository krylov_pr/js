/*
* Дана строка с последовательностью скобочек. Нужно проверить валидность.
* */
function valid (str) {
    const stack = []
    const revert = {
        '}': '{',
        ']': '[',
        ')': '(',
    }

    for (let i = 0; i < str.length; i++) {
        const current = str[i]
        if(endSymbol(current)) {
            if(revert[current] !== stack.pop()) return false
        } else {
            stack.push(current)
        }
    }
    return stack.length === 0
}
// проверка на закрывающую скобку
function endSymbol(s) {
    return ['}', ']', ')'].includes(s)
}

const str1 = '({[]})' // true
const str2 = '({})[]()' // true
const str3 = '({{}))' // false
console.log(valid(str1))
console.log(valid(str2))
console.log(valid(str3))