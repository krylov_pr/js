let user = {
    name: 'user1',
    age: 21
}
let us = new Proxy(user, {
    get(target, prop) {
        if(prop in target) {
            return target[prop]
        } else {
            return prop.split('_').map(p => target[p]).join(' ')
        }
    },
    set(target, prop, value) {
        if(prop in target) {
            console.log('set value')
            target[prop] = value
        }
    },
    has(target, prop) {
        return ['name', 'age'].includes(prop)
    },
    deleteProperty(target, prop) {
        console.log('delete prop')
        if(prop in target) {
            delete target[prop]
        }
    }
})

console.log(us)

/// function

let log = text => `[error] ${text}`
let lg = new Proxy(log, {
    apply(target, thisArg, argArray) {
        console.log('start func')
        return target.apply(thisArg, argArray).toUpperCase()
    }
})

class Person {
    constructor(name, age) {
        this.name = name
        this.age = age
    }
}

let PersonProxy = new Proxy(Person, {
    construct(target, argArray) {
        return new Proxy(new target(...argArray), {
            get(target, prop) {
                return `result:: ${target[prop]}`
            }
        })
    }
})

let person = new PersonProxy('henderson', 25)

const hideProp = (target, prefix = '_') => {
    return new Proxy(target, {
        has(obj, prop) {
            return (prop in obj) && !prop.startsWith(prefix)
        },
        ownKeys(obj, prop) {
            return Reflect.ownKeys(obj).filter(p => !p.startsWith(prefix))
        },
        get(obj, prop, receiver) {
            return prop in receiver ? obj[prop] : void 0
        }
    })
}

const user3 = hideProp({
    name: 'Henderson',
    age: 25,
    _uid: 4142145
})

const indexArray = new Proxy(Array, {
    construct(target, [argArray]) {
        const index = {}
        argArray.forEach(item => index[item.id] = item)

        return new Proxy(new target(...argArray), {
            get(arr, prop) {
                switch (prop) {
                    case "push": return item => {
                        index[item.id] = item
                        arr[prop].call(arr, item)
                    }
                    case "findById": return id => index[id]
                    default: return arr[prop]
                }
            }
        })
    }
})

const personArr = [
    {id: 11, name: 'name1', age: 21},
    {id: 22, name: 'name2', age: 22},
    {id: 33, name: 'name2', age: 23},
]
const personActive = new indexArray(personArr)
console.log(personActive)

