// Interface Segregation principle

class Animal {
    constructor(name) {
        this.name = name
    }
}

// Суть принципа в том, что описанные ниже методы swim fly walk выносятся в отдельные объекты и в дальнейшем мержутся с теми классами, в которыъ они допустимы.
// Было бы ошибкой описать все эти методы в классе Animal, т.к. при наследовани от Animal у дочерних классов были бы лишние методы, которые пришлось бы переопределять.
const swimmer = {
    swim () {
        console.log(`${this.name} умеет плавать`)
    }
}
const flier = {
    fly () {
        console.log(`${this.name} умеет летать`)
    }
}
const walker = {
    walk () {
        console.log(`${this.name} умеет ходить`)
    }
}

class Dog extends Animal {}
class Eagle extends Animal {}
class Whale extends Animal {}

Object.assign(Dog.prototype, walker, swimmer)
Object.assign(Eagle.prototype, walker, flier)
Object.assign(Whale.prototype, swimmer)

const dog = new Dog('Рэкс')

dog.walk()
dog.swim()

const eagle = new Eagle('Орел')

eagle.fly()
eagle.walk()

const whale = new Whale('Дельфин')

whale.swim()