// Open Close Principle

class Shape {
    constructor() {

    }

    area () {
        throw new Error('empty method area')
    }
}

class Square extends Shape {
    constructor(size) {
        super()
        this.size = size
    }

    area() {
        return this.size ** 2
    }
}

class Circle extends Shape {
    constructor(radius) {
        super()
        this.radius = radius
    }

    area() {
        return (this.radius ** 2) * Math.PI
    }
}

// Суть принципа в том, что логика определения площади фигур описана в самом классе фигуры.
// При добавлении новой фигуры, класс Калькулятор не меняется.
// Класс калькулятор открывается для базовой логики и закрывается для дальнейших модификаций.
class AreaCalculator {
    constructor(shapes = []) {
        this.shapes = shapes
    }

    sum () {
        return this.shapes.reduce((acc, shape) => {
            return acc += shape.area()
        }, 1)
    }
}

const calc = new AreaCalculator([
    new Square(20),
    new Circle(1),
    new Circle(5),
])

console.log(calc.sum())