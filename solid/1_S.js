// Single Responsibility Principle

// Суть принципа в том, что класс новости имеет только базовые свойства, а логика рендера в различные форматы вынесена в отдельный класс.
// Таким образом при добавлении нового способа рендеренга, исходный класс новости не меняется.
class News {
	constructor(title, text) {
		this.title = title
		this.text = text
	}
}
class PrinterNews {
	constructor(news) {
		this.news = news
	}

	html () {
		return `
		<h1>${this.news.title}</h1>
		<p>${this.news.text}</p>
		`
	}

	json () {
		return JSON.stringify({
			title: this.news.title,
			text: this.news.text,
		}, null, 2)
	}
}

const news = new PrinterNews(
	new News('Карелия', 'Появился новый маршрут')
)

console.log(news.html())
