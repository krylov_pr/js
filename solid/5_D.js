// Dependency inversion principle

class Fetch {
    request (url) {
        // return fetch(url).then(r => r.json())
        return Promise.resolve('data from fetch')
    }
}

class LocalStorage {
    get () {
        const dataFromLocalStorage = 'data from local storage'
        return dataFromLocalStorage
    }
}

class FetchClient {
    constructor() {
        this.fetch = new Fetch()
    }
    clientGet (key) {
        return this.fetch.request(key)
    }
}

class LocalStorageClient {
    constructor() {
        this.localStorage = new LocalStorage()
    }
    clientGet (key) {
        return this.localStorage.get(key)
    }
}

class DataBase {
    constructor(client) {
        this.client = client
    }

    clientGet (key) {
        return this.client.clientGet(key)
    }
}

// Суть принципа в том, что экземпляр класса DataBase содержит единый метод clientGet, при вызыве которого будет вызвана нужная логика.
// У каждого клиента логика своя и в класс DataBase передается только клиент и вызывается универсальный метод клиента clientGet.
// В этом унивирсальном методе clientGet у каждого класса может быть описана своя логика.
const db = new DataBase(new FetchClient())
const dbLocalStorage = new DataBase(new LocalStorageClient())

// console.log(db.getData('vk.com'))
console.log(db.clientGet('vk.com'))
console.log(dbLocalStorage.clientGet('key'))

