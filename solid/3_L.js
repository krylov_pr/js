// liskov substitution principle

class Component {

}
class ComponentTemplate extends Component{
    render () {
        return '<div>Component</div>'
    }
}

class ComponentBase extends Component{
    info () {
        return 'info'
    }
}

class Header extends ComponentTemplate {

}
class Footer extends ComponentTemplate {

}
class User extends ComponentBase {
    render () {
        throw new Error()
    }
}

function renderComponent(component) {
    console.log(component.render())
}

renderComponent(new Header())
renderComponent(new Footer())
renderComponent(new User())