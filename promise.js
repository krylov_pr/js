const src1 = "https://www.culture.ru/storage/images/901ae2f4e739d2c39d66b0311397b35c/14495f4c2a14445d9f17bc0e45640343.jpeg"
const src2 = "https://fishki.net/picsw/102008/01/village/005_village.jpg"
const src3 = "https://fishki.net/picsw/102008/01/village/034_village.jpg"

const img = new Image()
img.height = 100
img.src = src1

document.body.append(img)

img.addEventListener('load', () => {
    const img2 = new Image()
    img2.height = 100
    img2.src = src2

    document.body.append(img2)
})

// Promise - объект, имеющий три состояния
// ожидание (pending)
// выполнено успешно (fulfilled)
// неуспешно (rejected)

const pr = new Promise((resolve, reject) => {
    // вызов функции resolve - переведет промис в состояние "выполнено успешно"
    // вызов функции reject - переведет промис в состояние "выполнено неуспешно"
})

function delay(ms) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve()
        }, ms)
    })
}

const promise = delay(2000)

promise
    .then(() => { console.log(1) })
    .then(() => { console.log(2) })
    .then(() => { console.log(3) })