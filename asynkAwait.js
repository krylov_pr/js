let delay = (ms) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve()
        }, ms)
    })
}

let url = 'https://jsonplaceholder.typicode.com/todos/1'

// function fetchTodo() {
//     return delay(2000)
//         .then(() => fetch(url))
//         .then(res => res.json())
// }

// fetchTodo().then((data) => {
//     console.log(data)
// })

async function fetchTodo() {
    try {
        await delay(2000)
        const result = await fetch(url)
        const data = await result.json()
        console.log(data)
    } catch (e) {}
}

fetchTodo()


