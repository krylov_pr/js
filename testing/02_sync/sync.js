class Lodash {
    compact (array) {
        return array.filter(val => !!val)
    }

    groupBy (array, prop) {
        return array.reduce((acc, item) => {
            const key = typeof prop === "function" ? prop(item) : item[prop]
            if(!acc[key]) {
                acc[key] = []
            }
            acc[key].push(item)
            return acc
        }, {})
    }
}

const a = {
    name: 'ivan',
    age: 21,
    company: {
        title: 'henderson',
        getName: function() {}
    }
}
const obj = {
    number: 25
}
const b = {...a, ...obj}

const f = [{age: 1}, {age: 2}]

const t = [...f]

f[1].age = 5



b.age = 28

// b.age = 23

// console.log(t)

module.exports = Lodash