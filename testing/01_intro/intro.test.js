const {sum, nativeNull} = require('./intro')
describe('Sum function: ', () => {
    test('should return sum of two values', () => {
        expect(sum(3, 1)).toBe(4)
        // expect(sum(3, 1)).toEqual(4)
    })

    test('should return vulues correctly comparing to other values', () => {
        expect(sum(3, 2)).toBeGreaterThan(4) // > 4
        expect(sum(3, 2)).toBeGreaterThanOrEqual(5) // = 5
        expect(sum(3, 2)).toBeLessThan(6) // < 6
        expect(sum(3, 2)).toBeLessThanOrEqual(5) // = 5
    })

    test('should 2 float values correctly ', () => {
        expect(sum(0.2, 0.1)).toBeCloseTo(0.3) // сравнение на соответствие для нецелых чисел
    })
})

describe('Native null function:', () => {
    test('should return false value null', () => {
        expect(nativeNull()).toBe(null)
        expect(nativeNull()).toBeNull()
        expect(nativeNull()).toBeFalsy()
        expect(nativeNull()).toBeDefined()
        expect(nativeNull()).not.toBeTruthy()
        expect(nativeNull()).not.toBeUndefined()
    })
})

