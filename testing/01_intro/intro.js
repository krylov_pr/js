function expect (value) {
    return {
        toBe: exp => {
            if(value === exp) {
                console.log('Success')
            } else {
                console.log('Error')
            }
        }
    }
}

function sum (a, b) {
    return a + b
}

function nativeNull () {
    return null
}

// console.log(expect(sum(41, 1)).toBe(42))

module.exports = {sum, nativeNull}