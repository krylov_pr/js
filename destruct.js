const person = {
    name: 'henderson',
    age: 26,
    addr: {
        city: 'Moscow',
        home: 5
    },
    status: true,
    date: '25.05'
}

const {name: lastname = 'default name', age, addr: {city, home}, ...all} = person

console.log(city)

function getAddr({city, home}) {
    return `${city} ${home}`
}

console.log(getAddr(person.addr))
