const mapPage = {
    '#main': '.page-main',
    '#about': '.page-about',
}
let pageActive = ''

//window.addEventListener('hashchange', handlePage)
const nav = document.querySelector('#nav')

nav.addEventListener('click', (e) => {
    if(e.target.tagName === 'A') {
        e.preventDefault()

        const href = e.target.getAttribute('href')

        history.pushState({
            page: href
        }, 'newPage')

        handlePage(href)
    }
})

window.addEventListener('popstate', (e) => {
    if(e.state && e.state.page) {
        handlePage(e.state.page)
    }
})

function handlePage(hash) {
    const pageClass = mapPage[hash]

    if(pageClass) {
        const page = document.querySelector(pageClass)

        if(page) {
            if(pageActive) {
                pageActive.classList.add('d-none')
            }

            page.classList.remove('d-none')
            pageActive = page
        }
    }
}
handlePage()
