class SimpleMembership {
  constructor(name) {
    this.name = name
    this.price = 50
  }
}
class StandardMembership {
  constructor(name) {
    this.name = name
    this.price = 100
  }
}
class PremiumMembership {
  constructor(name) {
    this.name = name
    this.price = 150
  }
}


class Membership {
  static list = {
    simple: SimpleMembership,
    standard: StandardMembership,
    premium: PremiumMembership,
  }

  create(name, type = 'simple') {
    const Member = Membership.list[type] || Membership.list['simple']
    const member = new Member(name)
    member.type = type
    member.define = () => {
      console.log(`User ${name} (${type}) : ${member.price}`)
    }
    return member
  }
}

const user = new Membership()

// Суть метода в том, что для создания новых экземпляров не нужно каждый раз создавать new PremiumMembership()
// Достаточно user.create('Elena', 'premium')
const arr = [
  user.create('Ivan', 'standard'),
  user.create('Elena', 'premium'),
  user.create('Varvara')
]

arr.forEach(m => {
  m.define()
})

// console.log(arr)
