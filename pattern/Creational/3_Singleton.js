class Database {
    constructor(data) {
        if(Database.exist) {
            return Database.instance
        }
        Database.instance = this
        Database.exist = true
        this.data = data
    }

    getData () {
        return this.data
    }
}
// Суть паттерна в том, что в контексте будут храниться данные переданные в параметр при первой инициализации класса

const mongo = new Database('MongoDB')
console.log(mongo.getData())

const sql = new Database('MySql')
console.log(sql.getData())