// Суть - добавляет слой метаданных для существующих объектов
class Server {
    constructor(ip, port) {
        this.ip = ip
        this.port = port
    }

    get getUrl () {
        return `https://${this.ip}:${this.port}`
    }
}

function aws (server) {
    server.aws = true
    server.awsInfo = function() {
        return server.getUrl
    }
    return server
}

const s1 = aws(new Server('0.0.41.18', '8080'))
console.log(s1.aws)
console.log(s1.awsInfo())