// суть - чтобы создавать удобный интерфейс для взаимодействия с различными классами
// позволяет упростить взаимодействие

class Complaints {
    constructor() {
        this.list = []
    }
    reply (complaint) {}
    add (complaint) {
        this.list.push(complaint)
        return this.reply(complaint)
    }
}

class ProductComplaint extends Complaints {
    reply({id, customer, detail}) {
        return `${id}, Product: ${customer} (${detail})`
    }
}

class ServiceComplaint extends Complaints {
    reply({id, customer, detail}) {
        return `${id}, Service: ${customer} (${detail})`
    }
}

// Данный метод определяет к какому классу отнести объект, добавляет различные метаданные
class ComplaintRegistry {
    register (customer, type, detail) {
        const id = Date.now()
        let complaint
        if(type === 'product') {
            complaint = new ProductComplaint()
        } else {
            complaint = new ServiceComplaint()
        }
        return complaint.add({id, customer, detail})
    }
}

const registry = new ComplaintRegistry()
console.log(registry.register('Pavel', 'product', 'не доступен'))
console.log(registry.register('Maria', 'service', 'ошибка'))