function customFilter(array, func) {
    let result = []
    for(let i = 0; i < array.length; i++) {
        if(func(array[i])) {
            result.push(array[i])
        }
    }

    return result
}
const person = [
    {name: 'name1', status: 15}, {name: 'name2', status: 21}, {name: 'name3', status: 30}, {name: 'name4', status: 35}, {name: 'name5', status: 45}
]

const activePerson = customFilter(person, (p) => {
    return p.status >= 18 && p.status <=35
})
console.log(activePerson)
